# TRABAJO FINAL

El trabajo consiste en desarrollar los modelos que vienen a continuación:

### DATAFRAME1: vinos.csv
+ Contiene el análisis quimico de diferentes tipos de vino.
+ Se solicita aplicar kmeans a fin de encontrar clusters de vinos. Tambien determinar el numero de clustes óptimo.

### DATAFRAME2: diabetes.csv
+ Contiene datos de personas que fueron diagnosticadas de diabetes (variable Outcome = 1 significa que presenta diabetes, 0 lo contrario).
+ Se pide generar un modelo de regresión (determinar si aplica regresión logística o regresión lineal) y predecir para un par de casos).

### Presentación
> **Entregable:** codigo en `R` o en `python`<br>
> **Trabajo grupal:** máximo 3 personas<br>
> **Fecha de entrega:** sab 09 de febrero 10:00 hrs